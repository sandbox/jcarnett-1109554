<?php

/**
 * @file
 * Admin page callbacks for the og_vocab module.
 */

/**
 * List and manage vocabularies for a given group.
 */
function og_vocab_admin_overview_vocabularies($form, $form_state, $entity_type, $etid) {
  $group = og_get_group($entity_type, $etid);
  og_set_breadcrumb($entity_type, $etid, array(l(t('Group'), "$entity_type/$etid/group")));
  //$entity = og_load_entity_from_group($group->gid);

  if (user_access('add own group vocabulary')) {
    $form['add_vocab'] = array(
      '#markup' => l(t('Add vocabulary'), "group/$entity_type/$etid/admin/taxonomy/add", array('query' => drupal_get_destination())),
    );
  }
  $rows = array();
  $vocabularies = og_vocab_get_vocabularies($group->gid);
  foreach ($vocabularies as $vid => $vocabulary) {
    /*
    $types = array();
    foreach ($vocabulary->nodes as $type) {
      $node_type = node_type_get_name($type);
      $types[] = $node_type ? t($node_type) : t($type);
    }
    */
    $row = array(
      check_plain(t($vocabulary->name)),
      //check_plain(implode(', ', $types)),
    );
    if (user_access('edit own group vocabulary')) {
      $row[] = l(t('edit vocabulary'), "group/$entity_type/$etid/admin/taxonomy/{$vocabulary->vid}/edit", array('query' => drupal_get_destination()));
    }
    $row[] = l(t('list terms'), "group/$entity_type/$etid/admin/taxonomy/{$vocabulary->vid}");
    if (user_access('edit own group term')) {
      $row[] = l(t('add terms'), "group/$entity_type/$etid/admin/taxonomy/{$vocabulary->vid}/add");
    }
    $rows[] = $row;
  }
  if (!$rows) {
    $rows[] = array(array(
        'data' => t('No categories'),
        'colspan' => 5,
      ));
  }
  $header = array(t('Name'), /*t('Type'),*/ array(
      'data' => t('Operations'),
      'colspan' => 3,
    ));
  $form['table'] =  array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );
  return $form;
}
