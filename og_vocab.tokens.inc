<?php

/**
 * @file
 * Builds placeholder replacement tokens for the og_vocab module.
 */

/**
 * Implements hook_token_info().
 */
function og_vocab_token_info() {
  return array(
    'tokens' => array(
      'vocabulary' => array(
        'og-name' => array(
          'name' => t('Organic group'),
          'description' => t('The label of the organic group to which the category belongs.'),
        ),
        'og-gid' => array(
          'name' => t('Organic group ID'),
          'description' => t('The ID of the organic group to which the category belongs.'),
        ),
        'og-entity-type' => array(
          'name' => t('Organic group entity type'),
          'description' => t('The entity type of the organic group to which the category belongs.'),
        ),
        'og-etid' => array(
          'name' => t('Organic group entity ID'),
          'description' => t('The entity ID of the organic group to which the category belongs.'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function og_vocab_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  if ($type == 'vocabulary' && !empty($data['vocabulary'])) {
    $vocabulary = $data['vocabulary'];
    $gid = og_vocab_get_group($vocabulary->vid);
    if ($gid) {
      $group = og_load($gid);
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'og-name':
            $replacements[$original] = $sanitize ? check_plain($group->label) : $group->label;
            break;
          case 'og-gid':
            $replacements[$original] = $gid;
            break;
          case 'og-entity-type':
            $replacements[$original] = $group->entity_type;
            break;
          case 'og-etid':
            $replacements[$original] = $group->etid;
            break;
        }
      }
    }
    else {
      foreach ($tokens as $name => $original) {
        $replacements[$original] = '';
      }
    }
  }

  return $replacements;
}
